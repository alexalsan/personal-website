# Personal website (2020)
## Check first draft on [https://alexalmagro.vercel.app/](https://alexalmagro.vercel.app/)

This page was created with the goal of making it attractive both for mobile and desktop devices, following the "Mobile-first" methodology. It was implemented using Next.js, which offers Server-Side rendering. Server-Side rendering makes your website's navigation more fluent, and makes the elements on your page more readeable for the searh engines (SEO).

## Technologies used
* React
* JavaScript
* Next.js
* CSS
* HTML
* Figma
* Interface design
* Responsive design