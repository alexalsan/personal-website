import React from 'react';
import classes from './Certification.module.css';

const Certification = (props) => {
    return (
        <div className={classes.Certification}>
            <img alt="" src={props.certification.imgUrl}/>
            <div className={classes.Text}>
                <p className={classes.Name}>{props.certification.name}
                    <span className={classes.Date}>{props.certification.date}</span>
                </p>
                {
                    props.certification.hours ?
                        <p className={classes.Hours}>{props.certification.organization} 
                            <span className={classes.Organization}>({props.certification.hours} hours)</span>
                        </p>
                        :
                        <p className={classes.Hours}>{props.certification.organization}</p>
                }
                
                <button className="secondary"><a href={props.certification.link} target="_blank" rel="noopener noreferrer">
                    See certification    
                </a></button>
            </div>
        </div>
    );
};

export default Certification;