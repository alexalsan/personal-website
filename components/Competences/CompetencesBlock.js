import React from 'react';
import classes from './CompetencesBlock.module.css';

const CompetencesBlock = (props) => {
    return (
        <div className={classes.CompetenceBlock}>
            <h4>{props.block.title}</h4>
            <p className="small-subtitle">
                <ul>
                    {
                        props.block.competences.map( (comp) => {
                            return <li>{comp.name}
                                { comp.info ? <><br/><span className={classes.Info}>{comp.info}</span></> : "" }
                            </li>
                        })
                    }
                </ul>
            </p>
        </div>
    );
};

export default CompetencesBlock;