import React, { Component } from 'react';
import classes from './ContactForm.module.css';
import emailjs, { init } from 'emailjs-com';
import ContactInput from './ContactInput/ContactInput';

class ContactForm extends Component {
    constructor (props){
        super(props);
        this.state= {
            name: "",
            email: "",
            isEmailOk: false,
            msg: "",
            submitting: false,
            success: false,
            error: false
        };

        init("user_kDOX6J2T5zGcc7VAEgaSI");
    }

    onNameChange = (event) => { this.setState({name:event.target.value}); }

    onMsgChange = (event) => { this.setState({msg:event.target.value}); }

    onEmailChange = (event) => {
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        const isEmailOk = re.test(String(event.target.value).toLowerCase());

        this.setState({isEmailOk: isEmailOk, email:event.target.value});
    }

    submitForm = () => {
        this.setState({submitting:true, success:false});

        if (this.state.name==="" || this.state.msg==="" || !this.state.isEmailOk){
            return;
        }

        const emailParams = {
            name: this.state.name,
            email: this.state.email,
            message: this.state.msg
        };
      
        emailjs.send("service_lhxlq1u", "template_h7dlsj8", emailParams)
            .then( (response) => {
                this.setState({success:true, error:false});
            }, (error) => {
               this.setState({error:true, success:false});
            });
    }

    render () {
        return (
            <React.Fragment>
                <form>
                    <ContactInput
                        type="text"
                        idName="name"
                        label="Name and surname(s):"
                        changed={this.onNameChange}
                        showError={this.state.submitting && this.state.name===""}
                        errorMsg="Please fill in the name."
                    />

                    <ContactInput
                        type="email"
                        idName="email"
                        label="Email:"
                        changed={this.onEmailChange}
                        showError={this.state.submitting && !this.state.isEmailOk}
                        errorMsg="The email is invalid."
                    />

                    <ContactInput
                        type="textarea"
                        idName="msg"
                        label="Message:"
                        changed={this.onMsgChange}
                        showError={this.state.submitting && this.state.msg===""}
                        errorMsg="Please fill in the message."
                    />
                </form>

                { this.state.success &&
                        <p className={classes.Success}>Message successfully sent</p> }

                { this.state.error &&
                    <p className={classes.Success}>ERROR. Trying contacting directly via: alexalmagrosantos@gmail.com</p> }

                <button className="primary" onClick={this.submitForm}>Send message</button>
            </React.Fragment>
        );
    }    
};

export default ContactForm;