import React from 'react';
import classes from './ContactInput.module.css';
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
// import { faExclamation } from '@fortawesome/free-solid-svg-icons'

const ContactInput = (props) => {
    return (
        <div className={classes.Block}>
            <label className={classes.Label} htmlFor={props.idName}>{props.label}</label><br/>

            {/* We can render either an input (of type: text, email...) or a textarea */}
            {
                props.type === "textarea" ?
                    <textarea className={classes.Textarea} id={props.idName} name={props.idName} onChange={props.changed}/> :
                    <input className={classes.Input} type={props.type} id={props.idName} name={props.idName} onChange={props.changed}/>
            }
            
            {/* We show an error if the field is invalid and the user tries to submit */}
            {
                ( props.showError &&
                    <div className={classes.InvalidField}>
                        <img alt="error in field" src="/assets/exclamation.png" />
                        <p>{props.errorMsg}</p>
                    </div>
                )
            }
        </div>
    );
};

export default ContactInput;

// ( props.showError &&
//     <p className={classes.InvalidField}>
//         <FontAwesomeIcon icon={faExclamation} style={{marginRight:"10px"}}/>{props.errorMsg}
//     </p>
// )