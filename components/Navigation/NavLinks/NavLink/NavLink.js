import React from 'react';
import classes from './NavLink.module.css';
import Link from 'next/link';
import { useRouter } from "next/router";

const NavLink = (props) => {
    const router = useRouter();
    return (
        <Link href={props.url}>
            <a className={[classes.Link, router.pathname===props.url ? classes.Active : ""].join(" ")}>{props.title}</a>
        </Link>
    );
};

export default NavLink;