import React, { useState } from 'react';
import classes from './NavLinks.module.css';
import NavLink from './NavLink/NavLink';
import { NavLinksData } from '../../../data/Navigation/NavigationData';
import Link from 'next/link';

const NavLinks = (props) => {
    const [isMenuOpen, toggleMenu] = useState(false);

    return (
        <React.Fragment>
            <nav className={[classes.Desktop, props.light ? classes.LightMode : classes.DarkMode].join(" ")}>
                <Link href="/"><a className={classes.Logo}>
                    { props.light ?
                        <img alt="logo" src="/assets/logoLight.png"/>
                        : <img alt="logo" src="/assets/logoDark.png"/>
                    }
                </a></Link>
                { NavLinksData.map( (link) => {
                    return <NavLink key={link.title} url={link.url} title={link.title}/>;
                }) }
            </nav>

            <nav className={[classes.Mobile, classes.LightMode, isMenuOpen ? classes.Open : ""].join(" ")}>
                <div className={classes.Navbar}>
                    <div className={classes.Logo} onClick={() => toggleMenu(!isMenuOpen)}>
                        { isMenuOpen ?
                            <img alt="toggle menu" src="/assets/cross.png" style={{width: "25px", height: "25px"}}/>
                            : <img alt="toggle menu" src="/assets/menu.png"/>
                        }    
                    
                    </div>
                    <Link href="/"><a className={classes.Logo}>
                        <img alt="logo" src="/assets/logoLight.png"/>
                    </a></Link>
                </div>
                
                { isMenuOpen &&
                    <div className={classes.ToggleMenu}>
                        <div>
                            { NavLinksData.map( (link) => {
                                return <NavLink key={link.title} url={link.url} title={link.title}/>;
                            }) }
                        </div>
                    </div>
                }
            </nav>
        </React.Fragment>
    );
};

export default NavLinks;