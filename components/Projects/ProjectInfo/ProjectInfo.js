import React from 'react';
import classes from './ProjectInfo.module.css';
import Carousel from '../../../components/UI/Carousel/Carousel';
import ProjectSkills from '../ProjectSkills/ProjectSkills';

const ProjectInfo = (props) => {
    return (
        <div className={classes.Parent}>
            <h4>{props.project.name}
            {
                props.project.link &&
                <a href={props.project.link} target="_blank" rel="noopener noreferrer">Check project</a>
            }
            </h4>

            <p className="small-subtitle">{props.project.date}</p>

            <div className={classes.Wrapper}>
                <ProjectSkills skills={props.project.skills}/>
                <p className={classes.Description}>{props.project.description}</p>
                {
                    props.project.carousel && 
                        <div className={classes.CarouselWrapper}>
                            <Carousel imageArray={props.project.carousel} />
                        </div>
                }
            </div>
        </div>
    );
};

export default ProjectInfo;