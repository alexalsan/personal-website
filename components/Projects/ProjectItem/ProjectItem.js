import React from 'react';
import classes from './ProjectItem.module.css';

const ProjectItem = (props) => {
    return (
        <div className={classes.Item} style={{backgroundImage: `url(${props.imgUrl})`}} onClick={props.clicked}>
            <div className={classes.DarkLayer}>
                <p>{props.name}</p>
            </div>
        </div>
    );
};

export default ProjectItem;