import React from 'react';
import classes from './ProjectSkills.module.css';

const ProjectSkills = (props) => {
    return (
        <div className={classes.Parent}>
            <ul>
                {
                    props.skills.map( (skill) => {
                        return <li key={skill}>{skill}</li>
                    })
                }
            </ul>
        </div>
    );
};

export default ProjectSkills;