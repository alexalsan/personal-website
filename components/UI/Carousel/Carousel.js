import React, { useState } from 'react';
import FSImage from '../Full-screen/FSImage';
import classes from './Carousel.module.css';

const Carousel = (props) => {
    const [imageIndex, setImageIndex] = useState(0);
    const [isImageOpen, setIsImageOpen] = useState(false);

    const decreaseIndex = () => {
        if (imageIndex == 0){
            setImageIndex(props.imageArray.length - 1);
            return;
        }
        setImageIndex(imageIndex - 1);
    }

    const increaseIndex = () => {
        setImageIndex((imageIndex + 1) % props.imageArray.length);
    }
    
    const circles=[];
    for (let i=0; i<props.imageArray.length; i++){
        circles.push (<div key={i} className={(imageIndex===i) ? classes.Active : ""} onClick={() => setImageIndex(i)}></div>);
    }

    if (!props.imageArray[imageIndex]) {
        setImageIndex(0);
        setIsImageOpen(false)
    }

    return (
        <React.Fragment>
            { props.imageArray[imageIndex] &&
                
            <div className={classes.Parent}>
                <img className={classes.LeftArrow} alt="left arrow" src="/assets/leftArrow.png" onClick={decreaseIndex}/>
                <div className={classes.Image}>
                    <img alt="" src={props.imageArray[imageIndex].imgUrl} onClick={() => setIsImageOpen(true)}/>
                </div>
                <img className={classes.RightArrow} alt="right arrow" src="/assets/rightArrow.png" onClick={increaseIndex}/>
                <p className={classes.Caption}>{props.imageArray[imageIndex].caption}</p>
                <div className={classes.Circles}> { circles } </div>
            </div> }

            { isImageOpen &&
                <FSImage imgUrl={props.imageArray[imageIndex].imgUrl} clicked={() => setIsImageOpen(false)}/>
            }
        </React.Fragment>
    );
};

export default Carousel;