import React from 'react';
import classes from './FSImage.module.css';

const FSImage = (props) => {
    return (
        <div className={classes.Image} onClick={props.clicked}>
            <img alt="" src={props.imgUrl}/>
        </div>
    );
};

export default FSImage;