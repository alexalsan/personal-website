export const CertificationsData = [
    {
        name: "Professional Scrum Product Owner I (PSPO I)",
        organization: "Scrum.org",
        date: "2020",
        imgUrl: "/assets/certifications/PSPO.JPG",
        link: "https://www.scrum.org/certificates/543230"
    },
    {
        name: "Google Analytics for Beginners",
        hours: "6",
        organization: "Google",
        date: "2020",
        imgUrl: "/assets/certifications/GA_Beginners.JPG",
        link: "https://analytics.google.com/analytics/academy/certificate/31nLiBoeT0WSxchDGNpsSg"
    },
    {
        name: "Advanced JavaScript",
        hours: "6.5",
        organization: "Udemy",
        date: "2020",
        imgUrl: "/assets/certifications/Advanced_JavaScript.jpg",
        link: "https://www.udemy.com/certificate/UC-1a647cee-279c-45d2-9759-950365192fcd/"
    }
]