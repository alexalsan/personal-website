export const CompetencesData = [
    {
        title: "Front-end",
        competences: [
            { name:"JavaScript" },
            { name:"React" },
            { name:"Redux" },
            { name:"JQuery" },
            { name:"CSS" },
            { name:"HTML" },
            { name:"Figma" },
            { name:"Responsive Design & Mobile-First Websites" }
        ]
    },
    {
        title: "Back-end",
        competences: [
            { name:"C++" },
            { name:"SQL" },
            { name:"Firebase" },
            { name:"Unit testing" }
        ]
    },
    {
        title: "Other",
        competences: [
            { name:"Version control", info: "Git, Mercurial" },
            { name:"Experience in Agile teams", info: "Scrum, Kanban" }
        ]
    },
    {
        title: "Languages",
        competences: [
            { name:"Spanish", info:"Mother tongue" },
            { name:"English", info:"Fluent" },
            { name:"French", info:"Intermediate" },
            { name:"Italian", info:"Basic" }
        ]
    }
]