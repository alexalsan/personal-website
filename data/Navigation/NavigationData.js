export const NavLinksData = [
    {
        title: "Projects",
        url: "/projects" 
    },
    {
        title: "Competences",
        url: "/competences" 
    },
    // {
    //     title: "About",
    //     url: "/competences" 
    // },
    {
        title: "Contact",
        url: "/contact" 
    }
];