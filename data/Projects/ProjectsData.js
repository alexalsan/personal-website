export const projectsData = [
    {
        name: 'Emerge App',
        date: '2018 - 2019',
        description: `This project is born from the necessity of bringing new technologies to the agriculture sector in Spain. The final goal of the project was to create a mobile app which agriculture workers could use to predict the growth of harmful weeds using real-time data and mathematical algorithms. This makes it possible to significantly reduce the amount of herbicides used in the fields.
        
        The target users are smalls farmers which do not have access to sofisticated and expensive measures to control harmful weeds. Therefore, one of the main objectives was to make the app as accessible and user-friendly as possible, since the final users are not necessarily familiar with new technologies.

        This project was done individually, with the supervision and guidance of Jose Luis González (Dr., CSIC) and Pilar Martínez (Professor, Univerity of Córdoba).

        My main tasks during the year it took to create the app were to:
        • Coordinate the requirements with both CSIC, and University of Córdoba.
        • Design the interface. Since it was essential that the app was user-friedly, I created different prototypes and gathered feedback from testers.
        • Develop the application. The technologies used were JavaScript, CSS and HTML, all via the framework PhoneGap.
        • Write the technical documentation and user guide.
        • Present the project to a University committee. I decided to present this project as my final Bachelor thesis.`,
        skills: [
            'JavaScript',
            'JQuery',
            'CSS',
            'HTML',
            'PhoneGap (framework)',
            'Interface design',
            'Interface prototype',
            'Mobile app',
            'Responsive design'
        ],
        imgUrl: "/assets/projects/emerge1.jpg",
        carousel: [
            {
                caption: "Home menu",
                imgUrl: "/assets/projects/emerge1.jpg"
            },
            {
                caption: "Choosing location",
                imgUrl: "/assets/projects/emerge2.jpg"
            },
            {
                caption: "Getting values in real time",
                imgUrl: "/assets/projects/emerge3.jpg"
            },
            {
                caption: "Showing graph with growth prediction",
                imgUrl: "/assets/projects/emerge4.jpg"
            }
        ]
    },
    {
        name: 'Chatroom',
        link: 'https://chatroom-personal-project.web.app/',
        date: '2020',
        description: `It is a small personal project I implemented to get familiar with React. It is a small chat where you can post messages and see what other people posted as well. The project was born as a challenge of implementing a functional web app in a few days.
        
        It uses a variety of React & JavaScript features: dynamic elements, asynchronous programming,... The database used to store the messages is Firebase, due to its short learning curve and its compatibility with React.
        
        There are a lot of small details I took care of to make it user-friendly and comfortable. There is special focus on responsive design. In addition, Firebase's authentication system via Google accounts make it very secure to use, and assures that you can only delete or modify your own messages.`,
        imgUrl: "/assets/projects/chat3.JPG",
        skills: [
            'React',
            'JavaScript',
            'CSS',
            'HTML',
            'Firebase (database)',
            'Interface design',
            'Responsive design'
        ],
        carousel: [
            {
                caption: "Landing page (desktop)",
                imgUrl: "/assets/projects/chat1.JPG"
            },
            {
                caption: "Chat Room (desktop)",
                imgUrl: "/assets/projects/chat2.JPG"
            },
            {
                caption: "Chat Room (mobile)",
                imgUrl: "/assets/projects/chat3.JPG"
            },
            {
                caption: "Deleting a message (mobile)",
                imgUrl: "/assets/projects/chat4.JPG"
            }
        ]
    },
    {
        name: 'Andrana',
        link: 'https://andranaproject.com/',
        date: '2018',
        description: `Andrana is the first videogame of the Spanish start-up "Muquo". Andrana is a mobile game to play with friends, where you are divided in 2 teams and but don't know exactly who your allies are.

        The challenged in this project was to communicate the vibe and aesthetic of the videogame, placed in a dystopian future. This videogame is designed to be played physically with friends, but by using your phone. It was also essential to communicate this mix of digital and presential way of playing, which was the start-up's main feature.
        
        The amazing designed was created by the CEO of the start-up, and I implemented her vision using JavaScript and other web technologies. This was my first big front-end project, and I learned the fast pace required in a start-up.`,
        skills: [
            'JavaScript',
            'JQuery',
            'CSS',
            'HTML',
            'Responsive design'
        ],
        imgUrl: '/assets/projects/andrana1.JPG',
        carousel: [
            {
                caption: "Landing page",
                imgUrl: "/assets/projects/andrana1.JPG"
            },
            {
                caption: "Game instructions",
                imgUrl: "/assets/projects/andrana2.JPG"
            },
            {
                caption: "Story of the characters",
                imgUrl: "/assets/projects/andrana3.JPG"
            }
        ]
    },
    {
        name: 'Personal website',
        date: '2020',
        description: `This is the website you are on right now! It is the first big React project I have designed and implemented. In my experience, one of the key aspects of creating a functional and attractive website is to plan it carefully, both aesthetically and functionally. I worked on the designed using the online tool Figma, in order to create the feel of the page, and elements such as the logotype.
        
        This page was created with the goal of making it attractive both for mobile and desktop devices, following the "Mobile-first" methodology. It was implemented using Next.js, which offers Server-Side rendering. Server-Side rendering makes your website's navigation more fluent, and makes the elements on your page more readeable for the searh engines (SEO).
        
        This is a project I put a lot of care in, and which I wish to continue expanding, as my career grows and I keep learning and building my portfolio.`,
        skills: [
            'React',
            'JavaScript',
            'Next.js',
            'CSS',
            'HTML',
            'Figma',
            'Interface design',
            'Responsive design'
        ],
        imgUrl: '/assets/man_laptop.png'
    }
]