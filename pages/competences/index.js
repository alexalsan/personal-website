import React from 'react';
import classes from './CompetencesIndex.module.css';
import NavLinks from '../../components/Navigation/NavLinks/NavLinks';
import CompetencesBlock from '../../components/Competences/CompetencesBlock';
import { CompetencesData } from '../../data/Competences/CompetencesData';
import Certification from '../../components/Competences/Certification/Certification';
import { CertificationsData } from '../../data/Competences/CertificationsData';

const CompetencesIndex = (props) => {
    return (
        <React.Fragment>
            <NavLinks/>
            <div className="section-wrapper">
                <h3>Competences & certifications</h3>
                <p className="subtitle">You can also check my resume <a href="https://www.linkedin.com/in/alejandro-almagro-santos-353a60146/detail/overlay-view/urn:li:fsd_profileTreasuryMedia:(ACoAACNlP-8BgLZAjrYGwklb9HMjS5oeXNh_KA8,1600619374832)/" target="_blank" rel="noopener noreferrer">
                here</a></p>

                <div className={classes.CompetenceBlocks}>
                {
                    CompetencesData.map( (compBlock) => {
                        return <CompetencesBlock key={compBlock} block={compBlock}/>
                    })
                }   
                </div>

                <div className={classes.Certifications}>
                    <h4>Certifications</h4>
                    {
                        CertificationsData.map( certif => {
                            return <Certification key={certif} certification={certif}/>
                        })
                    }
                </div>
            </div>
        </React.Fragment>
    );
};

export default CompetencesIndex;

// <a href="https://www.linkedin.com/in/alejandro-almagro-santos-353a60146/detail/overlay-view/urn:li:fsd_profileTreasuryMedia:(ACoAACNlP-8BgLZAjrYGwklb9HMjS5oeXNh_KA8,1600619374832)/" target="_blank" rel="noopener noreferrer">
// <button className="secondary" style={{margin:'20px 0'}}>
//     here
// </button>
// </a>