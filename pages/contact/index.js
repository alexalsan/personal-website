import React from 'react';
import ContactForm from '../../components/ContactForm/ContactForm';
import NavLinks from '../../components/Navigation/NavLinks/NavLinks';
import classes from './ContactIndex.module.css';

const ContactIndex = (props) => {
    return (
        <React.Fragment>
            <NavLinks/>
            <div className="section-wrapper">
                <h3>Contact</h3>
                <p className="subtitle">Send me a message or contact me directly through <a href="https://www.linkedin.com/in/alejandro-almagro-santos-353a60146/" target="_blank" rel="noopener noreferrer">Linkedin</a></p>
                <div className={classes.FormWrapper}>
                    <ContactForm />
                </div>
            </div>
        </React.Fragment>
    );
};

export default ContactIndex;