import React from 'react';
import classes from './Index.module.css';
import NavLinks from '../components/Navigation/NavLinks/NavLinks';
import Link from 'next/link';

const Index = (props) => {
    return (
        <React.Fragment>
            <NavLinks light/>

            <div className={classes.Photo}></div>

            <div className={classes.Parent}>
                <div className={classes.Text}>
                    <h1>Alex Almagro</h1>
                    <h2>Software engineer</h2>
                </div>
                <div className={classes.Buttons}>
                    <Link href="/projects"><a><button className="primary big">Projects</button></a></Link>
                    <Link href="/contact"><a><button className="secondary big">Contact</button></a></Link>
                </div>
                <div className={classes.SocialMedia}>                
                    <div>
                        <a href="https://www.linkedin.com/in/alejandro-almagro-santos-353a60146/" target="_blank" rel="noopener noreferrer">
                            <img src="/assets/linkedin.png"/>
                        </a>
                    </div>
                    <div>
                        <a href="https://bitbucket.org/alexalsan/" target="_blank" rel="noopener noreferrer">
                            <img src="/assets/git.png"/>
                        </a>
                    </div>
                </div>
            </div>
        </React.Fragment>
    );
};

export default Index;

// <div className={[classes.mainDiv, "d-flex", "justify-content-start", "align-items-end"].join(' ')}>
//                         <p className={classes.Title}>
//                             Alex Almagro
//                         </p>
//                         <div className={classes.Logo}>
//                             <img alt="Logo" src={logo}/>
//                         </div>
//                     </div>
//                     <img src={mainPhoto} alt="" className={classes.MainPhoto}></img>
//                     <p className={classes.Subtitle}>Software engineer</p>

                
//                     <p className={classes.presentationText}>"I am a young engineer with passion for both creative and technical elements of the industry. I have international experience in both front-end and back-end technologies, always striving to achieve the best results.”</p>
//                     <div className={classes.ContactBlock} style={{marginRight:"10px"}}>
//                         <img className={classes.ContactIcon} style={{marginRight:"10px"}} alt="" src={email} />
//                         <p className={classes.ContactInfo}>alexalmagrosantos@gmail.com</p>
//                     </div>
//                     <a href="https://www.linkedin.com/in/alejandro-almagro-santos-353a60146/" target="_blank" rel="noopener noreferrer">
//                         <div className={classes.ContactBlock}>
//                             <img className={classes.ContactIcon} alt="" src={linkedin} />
//                         </div>
//                     </a>