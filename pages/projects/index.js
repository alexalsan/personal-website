import React, { useState, useRef } from 'react';
import classes from './ProjectsIndex.module.css';
import NavLinks from '../../components/Navigation/NavLinks/NavLinks';
import ProjectItem from '../../components/Projects/ProjectItem/ProjectItem';
import ProjectInfo from '../../components/Projects/ProjectInfo/ProjectInfo';
import { projectsData } from '../../data/Projects/ProjectsData';

const ProjectsIndex = (props) => {
    const [projectIndex, setProjectIndex] = useState(null);

    const infoDiv = useRef(null);

    const openDescription = (index) => {
        //If we click twice in the same item, we toggle it
        if (index === projectIndex){
            setProjectIndex(null);
            return;
        }

        infoDiv.current.scrollIntoView({behaviour: 'smooth'});
        setProjectIndex(index);
    }

    return (
        <React.Fragment>
            <NavLinks/>
            <div className="section-wrapper">
                <h3>Projects</h3>
                <p className="subtitle">Click to check out my Front-end projects!</p>
                <div className={classes.Projects}>
                    {
                        projectsData.map( (project, i) => {
                            return <ProjectItem key={project.name} imgUrl={project.imgUrl} name={project.name} clicked={() => openDescription(i)}/>
                        })
                    }
                </div> 

                <div ref={infoDiv}></div>
                { projectIndex!==null && <ProjectInfo project={projectsData[projectIndex]}/> }
            </div>
        </React.Fragment>
    );
};

export default ProjectsIndex;